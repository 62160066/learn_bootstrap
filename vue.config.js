module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160066/learn_bootstrap/'
    : '/'
}
